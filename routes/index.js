const express = require('express');
const router = express.Router();

// Need to replace with DB
const baseIceCreamList = require('../data/baseIceCream.json');
const condimentsList = require('../data/condiments.json');

// Home
router.get('/', (req, res, next) => {
    res.render('index', { title: 'Express' });
});

// List of ice create flavors
router.get('/list_base', (req, res, next) => {
    res.json({ data: baseIceCreamList.items, status: true, message: 'List of base IceCream list' });
});

// List of condiments
router.get('/list_flavours', (req, res, next) => {
    res.json({ data: condimentsList.items, status: true, message: 'List of flavours' });
});

module.exports = router;
